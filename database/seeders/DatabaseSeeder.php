<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder {


    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Harvey Mendoza',
            'email' => 'admin@admin.com',
            'password' => Hash::make('admin123'),
            'role' => 'Admin',
            'active' => '0',
        ]);

        DB::table('users')->insert([
            'name' => 'Agent',
            'email' => 'agent@agent.com',
            'password' => Hash::make('agent123'),
            'role' => 'Agent',
            'active' => '0',
        ]);
    }

}
