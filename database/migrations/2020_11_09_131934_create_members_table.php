<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->id();
            $table->string('member_approval_id')->nullable();
            $table->string('fname');
            $table->string('lname');
            $table->string('email')->unique();
            $table->string('nationality');
            $table->string('country_number');
            $table->string('phone_number');
            $table->string('location');
            $table->string('province');
            $table->string('program');
            $table->string('resedential_address');
            $table->string('current_visa');
            $table->string('agent');
            $table->string('assignee');
            $table->string('question1');
            $table->string('question2');
            $table->string('question3');
            $table->string('question4');
            $table->string('question5');
            $table->string('question6');
            $table->string('question7');
            $table->string('question8');
            $table->string('upload_application');
            $table->string('upload_passport');
            $table->string('upload_share_link');
            $table->string('upload_photo');
            $table->string('upload_marriage_certificate');
            $table->string('upload_child_birth_certificate');
            $table->string('upload_others');
            $table->string('comment');
            $table->string('agreement');
            $table->string('status');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
