<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    use HasFactory;

    protected $fillable = [
        'member_approval_id',
        'fname',
        'lname',
        'email',
        'nationality',
        'country_number',
        'phone_number',
        'location',
        'province',
        'program',
        'resedential_address',
        'current_visa',
        'agent',
        'assignee',
        'question1',
        'question2',
        'question3',
        'question4',
        'question5',
        'question6',
        'question7',
        'question8',
        'upload_application',
        'upload_passport',
        'upload_share_link',
        'upload_photo',
        'upload_marriage_certificate',
        'upload_child_birth_certificate',
        'upload_others',
        'comment',
        'agreement',
        'status'
    ];
}
