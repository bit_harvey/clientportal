<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;
use App\Models\User;

class MemberController extends Controller
{
    public function index()
    {
        $agents = User::where('role', 'Agent')->get();
        return view('application', compact('agents'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'member_approval_id' => 'nullable',
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|unique:members',
            'nationality' => 'required',
            'country_number' => 'required',
            'phone_number' => 'required',
            'location' => 'required',
            'province' => 'required',
            'program' => 'required',
            'resedential_address' => 'required',
            'current_visa' => 'required',
            'agent' => 'required',
            'assignee' => 'nullable',
            'question1' => 'required',
            'question2' => 'required',
            'question3' => 'required',
            'question4' => 'required',
            'question5' => 'required',
            'question6' => 'required',
            'question7' => 'required',
            'question8' => 'required',
            'upload_share_link' => 'required',
            'upload_others' => 'required',
            'comment' => 'required',
            'agreement' => 'required',
            'status' => 'nullable'
        ]);


        if($request->hasfile('upload_application'))
         {
            foreach($request->file('upload_application') as $upload_application)
            {
                $upload_application_name = time().'.'.$upload_application->extension();
                $upload_application->move(public_path('files'), $upload_application_name);  
                $upload_applications[] = $upload_application_name;  
            }

            $upload_application_image = json_encode($upload_applications);

         }

         if($request->hasfile('upload_passport'))
         {
            foreach($request->file('upload_passport') as $upload_passport)
            {
                $upload_passport_name = time().'.'.$upload_passport->extension();
                $upload_passport->move(public_path('files'), $upload_passport_name);  
                $upload_passports[] = $upload_passport_name;  
            }

            $upload_passport_image = json_encode($upload_passports);

         }

         if($request->hasfile('upload_photo'))
         {
            foreach($request->file('upload_photo') as $upload_photo)
            {
                $upload_photo_name = time().'.'.$upload_photo->extension();
                $upload_photo->move(public_path('files'), $upload_photo_name);  
                $upload_photos[] = $upload_photo_name;  
            }

            $upload_photo_image = json_encode($upload_photos);

         }

         if($request->hasfile('upload_marriage_certificate'))
         {
            foreach($request->file('upload_marriage_certificate') as $upload_marriage_certificate)
            {
                $upload_marriage_certificate_name = time().'.'.$upload_marriage_certificate->extension();
                $upload_marriage_certificate->move(public_path('files'), $upload_marriage_certificate_name);  
                $upload_marriage_certificates[] = $upload_marriage_certificate_name;  
            }

            $upload_marriage_certificate_image = json_encode($upload_marriage_certificates);

         }

         if($request->hasfile('upload_child_birth_certificate'))
         {
            foreach($request->file('upload_child_birth_certificate') as $upload_child_birth_certificate)
            {
                $upload_child_birth_certificate_name = time().'.'.$upload_child_birth_certificate->extension();
                $upload_child_birth_certificate->move(public_path('files'), $upload_marriage_certificate_name);  
                $upload_child_birth_certificates[] = $upload_child_birth_certificate_name;  
            }

            $upload_child_birth_certificate_image = json_encode($upload_child_birth_certificates);

         }

         if($request->hasfile('upload_others'))
         {
            foreach($request->file('upload_others') as $upload_other)
            {
                $upload_other_name = time().'.'.$upload_other->extension();
                $upload_other->move(public_path('files'), $upload_other_name);  
                $upload_others[] = $upload_other_name;  
            }

            $upload_other_image = json_encode($upload_others);

         }
         
        $member = new Member;
        $member->fname = $request->fname;
        $member->lname = $request->lname;
        $member->email = $request->email;
        $member->nationality = $request->nationality;
        $member->country_number = $request->country_number;
        $member->phone_number = $request->phone_number;
        $member->location = $request->location;
        $member->province = $request->province;
        $member->program = $request->program;
        $member->resedential_address = $request->resedential_address;
        $member->current_visa = $request->current_visa;
        $member->agent = $request->agent;
        $member->assignee = 'None';
        $member->question1 = $request->question1;
        $member->question2 = $request->question2;
        $member->question3 = $request->question3;
        $member->question4 = $request->question4;
        $member->question5 = $request->question5;
        $member->question6 = $request->question6;
        $member->question7 = $request->question7;
        $member->question8 = $request->question8;
        $member->upload_application = $upload_application_image;
        $member->upload_passport = $upload_passport_image;
        $member->upload_share_link = $request->upload_share_link;
        $member->upload_photo = $upload_photo_image;
        $member->upload_marriage_certificate = $upload_marriage_certificate_image;
        $member->upload_child_birth_certificate = $upload_child_birth_certificate_image;
        $member->upload_others = $upload_other_image;
        $member->comment = $request->comment;
        $member->agreement = $request->agreement;
        $member->status = '0';
        $member->save();

       
        return redirect()->route('application')
            ->with('success', 'Application Submitted Successfully.');
    }
}
