<?php

namespace App\Http\Livewire\ClientManager;

use Livewire\Component;

class ClientManagerMain extends Component
{
    public function render()
    {
        return view('livewire.client-manager.client-manager-main');
    }
}
