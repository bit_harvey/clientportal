<?php

namespace App\Http\Livewire\ClientManager;

use Livewire\Component;
use App\Models\Member;
use App\Models\User;
use Auth;
use DB;

class ClientManagerTable extends Component
{
    public $members;

    public function render()
    {
        if(Auth::user()->role == 'Admin'){
            $this->members  =
            DB::table('members')
            ->select('members.id','members.fname','members.lname','members.email', 'users.email as umail','members.status','members.assignee')
            ->leftJoin('users', 'users.id', '=', 'members.agent')
            ->where('status', 1)
            ->get();
        }else{
            $this->members = Member::where('agent', Auth::user()->id)
            ->get();
        }

        return view('livewire.client-manager.client-manager-table');
    }
}
