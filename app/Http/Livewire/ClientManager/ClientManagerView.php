<?php

namespace App\Http\Livewire\ClientManager;

use Livewire\Component;
use App\Models\Member;

class ClientManagerView extends Component
{
    public  $client;

    public function render()
    {
        $client_id= $this->client;
        return view('livewire.client-manager.client-manager-view', compact('client_id'));
    }

    public function mount($client)
    {
        
    }
}
