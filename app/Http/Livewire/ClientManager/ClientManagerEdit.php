<?php

namespace App\Http\Livewire\ClientManager;

use Livewire\Component;
use App\Models\Member;
use App\Models\User;
class ClientManagerEdit extends Component
{

   public $edit, $fname, $lname, $member_approval_id, $email, $nationality, $country_number, $phone_number, 
   $location, $province, $program, $resedential_address, $current_visa, $agent, $assignee,
   $question1,$question2,$question3,$question4,$question5,$question6,$question7,$question8,
   
   
   
   $update_member;

    public function render()
    {
        $allagents = User::where('role', 'Agent')->get();
        $allassignees = User::where('role', 'Admin')->get();
        return view('livewire.client-manager.client-manager-edit', compact('allagents', 'allassignees'));
    }

    public function mount($edit)
    {   


        $member = Member::where('id',$edit)->first();
        $this->member_approval_id = $member->member_approval_id;
        $this->fname = $member->fname;
        $this->lname = $member->lname;
        $this->email = $member->email;
        $this->nationality = $member->nationality;
        $this->country_number = $member->country_number;
        $this->phone_number = $member->phone_number;
        $this->location = $member->location;
        $this->province = $member->province;
        $this->program = $member->program;
        $this->resedential_address = $member->resedential_address;
        $this->current_visa = $member->current_visa;
        $this->agent = $member->agent;
        $this->assignee = $member->assignee;
        $this->question1 = $member->question1;
        $this->question2 = $member->question2;
        $this->question3 = $member->question3;
        $this->question4 = $member->question4;
        $this->question5 = $member->question5;
        $this->question6 = $member->question6;
        $this->question7 = $member->question7;
        $this->question8 = $member->question8;
    }

    public function update()
    {
         $this->validate([
            'member_approval_id' => 'nullable',
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required',
            'nationality' => 'required',
            'country_number' => 'required',
            'phone_number' => 'required',
            'location' => 'required',
            'province' => 'required',
            'program' => 'required',
            'resedential_address' => 'required',
            'current_visa' => 'required',
            'agent' => 'required',
            'assignee' => 'nullable',
            'question1' => 'required',
            'question2' => 'required',
            'question3' => 'required',
            'question4' => 'required',
            'question5' => 'required',
            'question6' => 'required',
            'question7' => 'required',
            'question8' => 'required'

        ]);
            
        $member = [
        'member_approval_id' =>  $this->member_approval_id,
        'fname' => $this->fname,
        'lname' => $this->lname,
        'email' => $this->email,
        'nationality' => $this->nationality,
        'country_number' => $this->country_number,
        'phone_number' => $this->phone_number,
        'location' => $this->location,
        'province' => $this->province,
        'program' => $this->program,
        'resedential_address' => $this->resedential_address,
        'current_visa' => $this->current_visa,
        'agent' => $this->agent,
        'assignee' => $this->assignee,
        'question1' => $this->question1,
        'question2' => $this->question2,
        'question3' => $this->question3,
        'question4' => $this->question4,
        'question5' => $this->question5,
        'question6' => $this->question6,
        'question7' => $this->question7,
        'question8' => $this->question8
        ];
        

        Member::find($this->edit)->update($member);

        session()->flash('success', 'Client Updated Successfully.');


    }

}
