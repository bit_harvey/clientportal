<?php

namespace App\Http\Livewire\Email;

use Livewire\Component;

class EmailMain extends Component
{
    public function render()
    {
        return view('livewire.email.email-main');
    }
}
