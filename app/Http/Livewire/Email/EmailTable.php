<?php

namespace App\Http\Livewire\Email;

use Livewire\Component;

class EmailTable extends Component
{
    public function render()
    {
        return view('livewire.email.email-table');
    }
}
