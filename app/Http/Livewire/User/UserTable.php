<?php

namespace App\Http\Livewire\User;

use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use App\Models\User;

class UserTable extends Component
{
    public $users, $client_id, $name, $email, $role, $active, $password, $user_id;
    public $updateMode = false;


    public function render()
    {
        $this->users = User::all();
        return view('livewire.user.user-table');
    }

    private function resetInputFields(){
        $this->client_id = '';
        $this->name =  '';
        $this->email =  '';
        $this->active =  '';
        $this->role =  '';
        $this->password =  '';
    }

    public function store()
    {
      $this->validate([
            'name' => 'required',
            'email' => 'required|email',
            'role' => 'required',
            'password' => 'required',
        ]);
        User::Create([
            'client_id' => NULL,
            'name' => $this->name,
            'email' => $this->email,
            'role' => $this->role,
            'active' => '0',
            'password' => Hash::make($this->password),
        ]);

        session()->flash('message', 'Users Created Successfully.');
        $this->resetInputFields();
        $this->emit('userStore'); // Close modal to using to jquery
    }
    
    public function edit($id)
    {
        $this->updateMode = true;
        $user = User::where('id',$id)->first();
        $this->user_id = $id;
        $this->name = $user->name;
        $this->email = $user->email;
    }

    
    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();
    }
    public function update()
    {
        $validatedDate = $this->validate([
            'name' => 'required',
            'email' => 'required|email',
        ]);
        if ($this->user_id) {
            $user = User::find($this->user_id);
            $user->update([
                'name' => $this->name,
                'email' => $this->email,
            ]);
            $this->updateMode = false;
            session()->flash('message', 'Users Updated Successfully.');
            $this->resetInputFields();
        }
    }

    public function delete($id)
    {
        if($id){
            User::where('id',$id)->delete();
            session()->flash('message', 'Users Deleted Successfully.');
        }

       
    }
}
