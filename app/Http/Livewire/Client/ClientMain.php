<?php

namespace App\Http\Livewire\Client;

use Livewire\Component;
use App\Models\Member;
use Auth;
class ClientMain extends Component
{
    public $members;

    public function render()
    {

 
    $this->members = Member::where('id', Auth::user()->client_id)->get();

        return view('livewire.client.client-main');
    }
}
