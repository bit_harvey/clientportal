<?php

namespace App\Http\Livewire\Applicant;

use Livewire\Component;
use App\Models\Member;
use App\Models\User;
use DB;
use Auth;

class ApplicantView extends Component
{
    
    public  $member, $client_id, $name, $email, $role, $active, $password; 
    public $updateMode = false;

    public function mount($member){

    }

    public function render()
    {
        $member = $this->member;

        if(Auth::user()->role == 'Admin'){
            $applicants=DB::table('members')
            ->leftJoin('users', 'users.id', '=', 'members.agent')
            ->where('members.id', '=', $member)
            ->select('members.*','users.email as umail')
            ->get();
        }else{
            $applicants=DB::table('members')
            ->where('members.id', '=', $member)
            ->where('members.agent', '=', auth::user()->id)
            ->select('members.*')
            ->get();
        }

        return view('livewire.applicant.applicant-view', compact('applicants'));
    }

    private function resetInput()
    {
        $this->client_id = '';
        $this->name =  '';
        $this->email =  '';
        $this->name =  '';
        $this->role =  '';
        $this->password =  '';
    }

    public function store()
    {
      $this->validate([
            'client_id' => 'required',
            'name' => 'required',
            'email' => 'required|email',
            'role' => 'required',
            'active' => 'required',
            'password' => 'required',
        ]);
        User::Create([
            'client_id' => $this->client_id,
            'name' => $this->name,
            'email' => $this->email,
            'role' => $this->role,
            'active' => $this->active,
            'password' => Hash::make($this->password),
        ]);

        session()->flash('message', 'Users Created Successfully.');
        
        $this->resetInput();

        $this->emit('approvedmodal_close'); 

    }

    public function edit($id)
    {
        $this->updateMode = true;
        $user = Member::where('id',$id)->first();
        $this->applicant_id = $id;
        $this->name = $user->name;
        $this->email = $user->email;
    }

}
