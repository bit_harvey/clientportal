<?php

namespace App\Http\Livewire\Applicant;

use Livewire\Component;



class ApplicantMain extends Component
{


    public function render()
    {
        return view('livewire.applicant.applicant-main');
    }
}
