<?php

namespace App\Http\Livewire\Applicant;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use Livewire\Component;
use App\Models\Member;
use App\Models\User;
use Auth;
use DB;


class ApplicantTable extends Component
{

    public $members, $name, $email, $role ,$application_id, $active, $password, $client_id, $agents, $agent_id;
    public $updateMode = false;

    public function render()
    {

        if(Auth::user()->role == 'Admin'){
            $this->members  =
            DB::table('members')
            ->select('members.id','members.fname','members.lname','members.email', 'users.email as umail','members.status','members.assignee')
            ->leftJoin('users', 'users.id', '=', 'members.agent')
            ->where('status', '!=', 1)
            ->get();
        }else{
            $this->members = Member::where('agent', Auth::user()->id)
            ->get();
        }

        $this->agents = User::where('role', 'Agent')->get();

        return view('livewire.applicant.applicant-table');
    }

    public function approved($id)
    {
        $random =Str::random(9);
        $this->updateMode = true;
        $applicant = Member::where('id',$id)->first();
        $this->applicant_id = $id;
        $this->client_id = $id;
        $this->name = $applicant->fname;
        $this->email = $applicant->email;
        $this->role = 'Client';
        $this->active = '1';
        $this->password = $random;
        
       
    }

    private function resetInputFields(){
        $this->client_id = '';
        $this->name =  '';
        $this->email =  '';
        $this->active =  '';
        $this->role =  '';
        $this->password =  '';
    }

    public function approved_store()
    {
      $this->validate([
            'client_id' => 'required',
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'role' => 'required',
            'active' => 'required',
            'password' => 'required',
        ]);
        User::Create([
            'client_id' => $this->client_id,
            'name' => $this->name,
            'email' => $this->email,
            'role' => $this->role,
            'active' => $this->active,
            'password' => Hash::make($this->password),
        ]);

      
        $status = Member::find($this->client_id,);
        $status->status = '1';
        $status->assignee = Auth::user()->email;
        $status->save();
        
        $details = [
            'email' => $this->email,
            'password' => $this->password,
        ];

        \Mail::to($this->email)->send(new \App\Mail\UserCredential($details));

        session()->flash('message', 'Users Created Successfully.');
        $this->resetInputFields();
        $this->emit('userStore'); // Close modal to using to jquery
    }

    public function assign($id)
    {

     
        $this->updateMode = true;
        $applicant = Member::where('id',$id)->first();
        $this->client_id = $id;
       
    }

    public function assign_store()
    {

        $this->validate([
            'client_id' => 'required',
            'agent_id' => 'required',
        ]);
     
        $status = Member::find($this->client_id,);
        $status->agent = $this->agent_id;
        $status->save();
        


        session()->flash('message', 'Assign Agent Successfully.');
        $this->resetInputFields();
        $this->emit('userStore'); // Close modal to using to jquery
       
    }

    public function decline($id)
    {

     
        $this->updateMode = true;
        $applicant = Member::where('id',$id)->first();
        $this->applicant_id = $id;
        $this->client_id = $id;
       
    }

    public function decline_store()
    {

        $this->validate([
            'client_id' => 'required',
        ]);
     
        $status = Member::find($this->client_id,);
        $status->status = '3';
        $status->agent = Auth::user()->id;
        $status->save();
        

        session()->flash('message', 'Decline Successfully.');
        $this->resetInputFields();
        $this->emit('userStore'); // Close modal to using to jquery
       
    }


    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();
    }

}
