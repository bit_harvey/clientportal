<?php

use Illuminate\Support\Facades\Route;

use App\Http\Livewire\Applicant\ApplicantMain;
use App\Http\Livewire\Applicant\ApplicantView;
use App\Http\Livewire\ClientManager\ClientManagerMain;
use App\Http\Livewire\ClientManager\ClientManagerView;

use App\Http\Livewire\Client\ClientMain;
use App\Http\Livewire\Admin\AdminMain;
use App\Http\Livewire\User\UserMain;
use App\Http\Livewire\Email\EmailMain;

use App\Http\Middleware\AdminMiddleware;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
}); 

Auth::routes();

Route::get('/online-application',  [App\Http\Controllers\MemberController::class, 'index'])->name('application');
Route::post('/online-application/store',  [App\Http\Controllers\MemberController::class, 'store'])->name('application.store');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/applicant', ApplicantMain::class)->name('applicant');
    Route::get('/applicant/manage/{member}', ApplicantView::class)->name('applicant.view');
    Route::get('/client-manager', ClientManagerMain::class)->name('client-manager');
    Route::get('/client-manager/view/{client}', ClientManagerView::class)->name('client-manager.view');

    Route::get('/client', ClientMain::class)->name('client');

    Route::get('/admin', AdminMain::class)->name('admin')->middleware(AdminMiddleware::class);
    Route::get('/admin/user', UserMain::class)->name('user')->middleware(AdminMiddleware::class);
    Route::get('/admin/email', EmailMain::class)->name('email')->middleware(AdminMiddleware::class);


    Route::get('/add-application',  [App\Http\Controllers\ApplicationController::class, 'index'])->name('add-application');
    Route::post('/add-application/store',  [App\Http\Controllers\ApplicationController::class, 'store'])->name('add-application.store');
    
});