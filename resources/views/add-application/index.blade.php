@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="main-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('applicant')}}">Application</a></li>
              <li class="breadcrumb-item active">Add Applicant</li>
            </ol>
          </nav>
          <!-- /Breadcrumb -->

            <div class="card">
                <div class="card-header">{{ __('Online Registration') }}</div>

                <div class="card-body">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ session()->get('success') }}</p>
                        </div>
                    @endif
                
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="POST" action="{{ route('add-application.store')}}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="fname" class="col-md-4 col-form-label text-md-right">{{ __('Firstname') }}</label>

                            <div class="col-md-6">
                                <input id="fname" type="text" class="form-control @error('fname') is-invalid @enderror" name="fname" value="{{ old('fname') }}" required autocomplete="fname" autofocus>

                                @error('fname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="lname" class="col-md-4 col-form-label text-md-right">{{ __('Lastname') }}</label>

                            <div class="col-md-6">
                                <input id="lname" type="text" class="form-control @error('lname') is-invalid @enderror" name="lname" value="{{ old('lname') }}" required autocomplete="lname" autofocus>

                                @error('lname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nationality" class="col-md-4 col-form-label text-md-right">{{ __('Nationality') }}</label>

                            <div class="col-md-6">
                            <select  class="form-control @error('nationality') is-invalid @enderror" name="nationality"  required>
                                    <option value="">Select nationality</option>
                                    <option value="afghan">Afghan</option>
                                    <option value="albanian">Albanian</option>
                                    <option value="algerian">Algerian</option>
                                    <option value="american">American</option>
                                    <option value="andorran">Andorran</option>
                                    <option value="angolan">Angolan</option>
                                    <option value="antiguans">Antiguans</option>
                                    <option value="argentinean">Argentinean</option>
                                    <option value="armenian">Armenian</option>
                                    <option value="australian">Australian</option>
                                    <option value="austrian">Austrian</option>
                                    <option value="azerbaijani">Azerbaijani</option>
                                    <option value="bahamian">Bahamian</option>
                                    <option value="bahraini">Bahraini</option>
                                    <option value="bangladeshi">Bangladeshi</option>
                                    <option value="barbadian">Barbadian</option>
                                    <option value="barbudans">Barbudans</option>
                                    <option value="batswana">Batswana</option>
                                    <option value="belarusian">Belarusian</option>
                                    <option value="belgian">Belgian</option>
                                    <option value="belizean">Belizean</option>
                                    <option value="beninese">Beninese</option>
                                    <option value="bhutanese">Bhutanese</option>
                                    <option value="bolivian">Bolivian</option>
                                    <option value="bosnian">Bosnian</option>
                                    <option value="brazilian">Brazilian</option>
                                    <option value="british">British</option>
                                    <option value="bruneian">Bruneian</option>
                                    <option value="bulgarian">Bulgarian</option>
                                    <option value="burkinabe">Burkinabe</option>
                                    <option value="burmese">Burmese</option>
                                    <option value="burundian">Burundian</option>
                                    <option value="cambodian">Cambodian</option>
                                    <option value="cameroonian">Cameroonian</option>
                                    <option value="canadian">Canadian</option>
                                    <option value="cape verdean">Cape Verdean</option>
                                    <option value="central african">Central African</option>
                                    <option value="chadian">Chadian</option>
                                    <option value="chilean">Chilean</option>
                                    <option value="chinese">Chinese</option>
                                    <option value="colombian">Colombian</option>
                                    <option value="comoran">Comoran</option>
                                    <option value="congolese">Congolese</option>
                                    <option value="costa rican">Costa Rican</option>
                                    <option value="croatian">Croatian</option>
                                    <option value="cuban">Cuban</option>
                                    <option value="cypriot">Cypriot</option>
                                    <option value="czech">Czech</option>
                                    <option value="danish">Danish</option>
                                    <option value="djibouti">Djibouti</option>
                                    <option value="dominican">Dominican</option>
                                    <option value="dutch">Dutch</option>
                                    <option value="east timorese">East Timorese</option>
                                    <option value="ecuadorean">Ecuadorean</option>
                                    <option value="egyptian">Egyptian</option>
                                    <option value="emirian">Emirian</option>
                                    <option value="equatorial guinean">Equatorial Guinean</option>
                                    <option value="eritrean">Eritrean</option>
                                    <option value="estonian">Estonian</option>
                                    <option value="ethiopian">Ethiopian</option>
                                    <option value="fijian">Fijian</option>
                                    <option value="filipino">Filipino</option>
                                    <option value="finnish">Finnish</option>
                                    <option value="french">French</option>
                                    <option value="gabonese">Gabonese</option>
                                    <option value="gambian">Gambian</option>
                                    <option value="georgian">Georgian</option>
                                    <option value="german">German</option>
                                    <option value="ghanaian">Ghanaian</option>
                                    <option value="greek">Greek</option>
                                    <option value="grenadian">Grenadian</option>
                                    <option value="guatemalan">Guatemalan</option>
                                    <option value="guinea-bissauan">Guinea-Bissauan</option>
                                    <option value="guinean">Guinean</option>
                                    <option value="guyanese">Guyanese</option>
                                    <option value="haitian">Haitian</option>
                                    <option value="herzegovinian">Herzegovinian</option>
                                    <option value="honduran">Honduran</option>
                                    <option value="hungarian">Hungarian</option>
                                    <option value="icelander">Icelander</option>
                                    <option value="indian">Indian</option>
                                    <option value="indonesian">Indonesian</option>
                                    <option value="iranian">Iranian</option>
                                    <option value="iraqi">Iraqi</option>
                                    <option value="irish">Irish</option>
                                    <option value="israeli">Israeli</option>
                                    <option value="italian">Italian</option>
                                    <option value="ivorian">Ivorian</option>
                                    <option value="jamaican">Jamaican</option>
                                    <option value="japanese">Japanese</option>
                                    <option value="jordanian">Jordanian</option>
                                    <option value="kazakhstani">Kazakhstani</option>
                                    <option value="kenyan">Kenyan</option>
                                    <option value="kittian and nevisian">Kittian and Nevisian</option>
                                    <option value="kuwaiti">Kuwaiti</option>
                                    <option value="kyrgyz">Kyrgyz</option>
                                    <option value="laotian">Laotian</option>
                                    <option value="latvian">Latvian</option>
                                    <option value="lebanese">Lebanese</option>
                                    <option value="liberian">Liberian</option>
                                    <option value="libyan">Libyan</option>
                                    <option value="liechtensteiner">Liechtensteiner</option>
                                    <option value="lithuanian">Lithuanian</option>
                                    <option value="luxembourger">Luxembourger</option>
                                    <option value="macedonian">Macedonian</option>
                                    <option value="malagasy">Malagasy</option>
                                    <option value="malawian">Malawian</option>
                                    <option value="malaysian">Malaysian</option>
                                    <option value="maldivan">Maldivan</option>
                                    <option value="malian">Malian</option>
                                    <option value="maltese">Maltese</option>
                                    <option value="marshallese">Marshallese</option>
                                    <option value="mauritanian">Mauritanian</option>
                                    <option value="mauritian">Mauritian</option>
                                    <option value="mexican">Mexican</option>
                                    <option value="micronesian">Micronesian</option>
                                    <option value="moldovan">Moldovan</option>
                                    <option value="monacan">Monacan</option>
                                    <option value="mongolian">Mongolian</option>
                                    <option value="moroccan">Moroccan</option>
                                    <option value="mosotho">Mosotho</option>
                                    <option value="motswana">Motswana</option>
                                    <option value="mozambican">Mozambican</option>
                                    <option value="namibian">Namibian</option>
                                    <option value="nauruan">Nauruan</option>
                                    <option value="nepalese">Nepalese</option>
                                    <option value="new zealander">New Zealander</option>
                                    <option value="ni-vanuatu">Ni-Vanuatu</option>
                                    <option value="nicaraguan">Nicaraguan</option>
                                    <option value="nigerien">Nigerien</option>
                                    <option value="north korean">North Korean</option>
                                    <option value="northern irish">Northern Irish</option>
                                    <option value="norwegian">Norwegian</option>
                                    <option value="omani">Omani</option>
                                    <option value="pakistani">Pakistani</option>
                                    <option value="palauan">Palauan</option>
                                    <option value="panamanian">Panamanian</option>
                                    <option value="papua new guinean">Papua New Guinean</option>
                                    <option value="paraguayan">Paraguayan</option>
                                    <option value="peruvian">Peruvian</option>
                                    <option value="philippines">Philippines</option>
                                    <option value="polish">Polish</option>
                                    <option value="portuguese">Portuguese</option>
                                    <option value="qatari">Qatari</option>
                                    <option value="romanian">Romanian</option>
                                    <option value="russian">Russian</option>
                                    <option value="rwandan">Rwandan</option>
                                    <option value="saint lucian">Saint Lucian</option>
                                    <option value="salvadoran">Salvadoran</option>
                                    <option value="samoan">Samoan</option>
                                    <option value="san marinese">San Marinese</option>
                                    <option value="sao tomean">Sao Tomean</option>
                                    <option value="saudi">Saudi</option>
                                    <option value="scottish">Scottish</option>
                                    <option value="senegalese">Senegalese</option>
                                    <option value="serbian">Serbian</option>
                                    <option value="seychellois">Seychellois</option>
                                    <option value="sierra leonean">Sierra Leonean</option>
                                    <option value="singaporean">Singaporean</option>
                                    <option value="slovakian">Slovakian</option>
                                    <option value="slovenian">Slovenian</option>
                                    <option value="solomon islander">Solomon Islander</option>
                                    <option value="somali">Somali</option>
                                    <option value="south african">South African</option>
                                    <option value="south korean">South Korean</option>
                                    <option value="spanish">Spanish</option>
                                    <option value="sri lankan">Sri Lankan</option>
                                    <option value="sudanese">Sudanese</option>
                                    <option value="surinamer">Surinamer</option>
                                    <option value="swazi">Swazi</option>
                                    <option value="swedish">Swedish</option>
                                    <option value="swiss">Swiss</option>
                                    <option value="syrian">Syrian</option>
                                    <option value="taiwanese">Taiwanese</option>
                                    <option value="tajik">Tajik</option>
                                    <option value="tanzanian">Tanzanian</option>
                                    <option value="thai">Thai</option>
                                    <option value="togolese">Togolese</option>
                                    <option value="tongan">Tongan</option>
                                    <option value="trinidadian or tobagonian">Trinidadian or Tobagonian</option>
                                    <option value="tunisian">Tunisian</option>
                                    <option value="turkish">Turkish</option>
                                    <option value="tuvaluan">Tuvaluan</option>
                                    <option value="ugandan">Ugandan</option>
                                    <option value="ukrainian">Ukrainian</option>
                                    <option value="uruguayan">Uruguayan</option>
                                    <option value="uzbekistani">Uzbekistani</option>
                                    <option value="venezuelan">Venezuelan</option>
                                    <option value="vietnamese">Vietnamese</option>
                                    <option value="welsh">Welsh</option>
                                    <option value="yemenite">Yemenite</option>
                                    <option value="zambian">Zambian</option>
                                    <option value="zimbabwean">Zimbabwean</option>
                                </select>

                                @error('nationality')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="country_number" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                            <div class="col-md-3">
                                <select id="country_number" class="form-control @error('country_number') is-invalid @enderror" name="country_number" value="" required>
                                    <option value="1">1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>

                                @error('country_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                

                            <div class="col-md-3">
                            <input id="phone_number" type="number" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="" required autocomplete="phone_number">


                                @error('phone_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="location" class="col-md-4 col-form-label text-md-right">{{ __('My Location') }}</label>

                            <div class="col-md-6">
                                <select id="location" class="form-control @error('location') is-invalid @enderror" name="location" value="" required>
                                    <option value="1">1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>

                                @error('nationality')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="province" class="col-md-4 col-form-label text-md-right">{{ __('If you are in Thailand, please select what province you are in.') }}</label>

                            <div class="col-md-6">
                                <select id="province" class="form-control @error('province') is-invalid @enderror" name="province" value="" required>
                                    <option value="1">1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>

                                @error('province')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="program" class="col-md-4 col-form-label text-md-right">{{ __('My Preferred Program') }}</label>

                            <div class="col-md-6">
                                <select id="program" class="form-control @error('program') is-invalid @enderror" name="program" value="" required>
                                    <option value="1">1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>

                                @error('program')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="resedential_address" class="col-md-4 col-form-label text-md-right">{{ __('Residential Address') }}</label>
                            

                            <div class="col-md-6">
                            <input id="resedential_address" type="text" class="form-control @error('resedential_address') is-invalid @enderror" name="resedential_address" value="" required autocomplete="resedential_address" autofocus>
                              <small>Please provide either a home country address or a Thailand address. Please write complete address include, district, city, province, etc.</small>
                                @error('resedential_address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <label for="current_visa" class="col-md-4 col-form-label text-md-right">{{ __('Current Visa') }}</label>

                            <div class="col-md-6">
                                <select id="current_visa" class="form-control @error('current_visa') is-invalid @enderror" name="current_visa" value="" required>
                                    <option value="">Please Select</option>
                                    <option value="I dont have a visa">I dont have a visa</option>
                                    <option value="I am holding tourist visa">I am holding tourist visa</option>
                                    <option value="I am holding education visa now">I am holding education visa now</option>
                                    <option value="I am holding work visa now">I am holding education visa now</option>
                                    <option value="I am holding non 0 visa now">I am holding non 0 visa now</option>
                                    <option value="Others">Others</option>
                                </select>

                                @error('current_visa')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="agent" class="col-md-4 col-form-label text-md-right">{{ __('Agent') }}</label>

                            <div class="col-md-6">
                                <select id="agent" class="form-control @error('agent') is-invalid @enderror" name="agent" value="" required>
                                    <option value="{{Auth::user()->id}}">{{Auth::user()->name}}</option>
                                    @foreach($agents as $agent)
                                    <option value="{{ $agent->id }}">{{ $agent->name }}</option>
                                    @endforeach
                                </select>

                                @error('agent')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="question1" class="col-md-4 col-form-label text-md-right">{{ __('If you have a visa, please provide place of issue and expiry date:') }}</label>
                            

                            <div class="col-md-6">
                            <input id="question1" type="text" class="form-control @error('question1') is-invalid @enderror" name="question1" value="{{ old('question1') }}" required autocomplete="question1" autofocus>
                              <small>Some visa issued from Thai Embassy are not allowed to cancel within Thailand.</small>
                                @error('question1')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="question2" class="col-md-4 col-form-label text-md-right">{{ __('Applicant have at least 3 empty pages in my passport (not include endorsement page)') }}</label>
                            

                            <div class="col-md-6">
                            <select id="question2" class="form-control @error('question2') is-invalid @enderror" name="question2" value="" required>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                    <option value="Others">Others</option>
                                </select>
                              <small>Elite Privilege Entry (PE) Visa is a sticker that will take up 1 full page of your passport.</small>
                                @error('question2')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="question3" class="col-md-4 col-form-label text-md-right">{{ __('Where would you prefer to affix your Elite visa? (this is not a confirmation of your option)') }}</label>
                            

                            <div class="col-md-6">
                            <select id="question3" class="form-control @error('question3') is-invalid @enderror" name="question3" value="" required>
                                    <option value="1">1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="question4" class="col-md-4 col-form-label text-md-right">{{ __('Do you own a property in Thailand?') }}</label>
                            

                            <div class="col-md-6">
                            <select id="question4" class="form-control @error('question4') is-invalid @enderror" name="question4" value="" required>
                                    <option value="1">1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                                <small>Elite Privilege Entry (PE) Visa is a sticker that will take up 1 full page of your passport.</small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="question5" class="col-md-4 col-form-label text-md-right">{{ __('Have you ever overstay in Thailand for more than once?') }}</label>
                            

                            <div class="col-md-6">
                            <select id="question5" class="form-control @error('question5') is-invalid @enderror" name="question5" value="" required>
                                    <option value="1">1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                                <small>Elite Privilege Entry (PE) Visa is a sticker that will take up 1 full page of your passport.</small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="question6" class="col-md-4 col-form-label text-md-right">{{ __('Have you ever overstay in Thailand for more than once?') }}</label>

                            <div class="col-md-6">
                                <div class="checkbox">
                                     <label><input type="checkbox" name="question6" value="Myself">Myself</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="question6" value="My Spouse">My Spouse</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="question6" value="My Children">My Children</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="question6" value="Others">Others</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="question7" class="col-md-4 col-form-label text-md-right">{{ __('Name of contact person or introducer, if any.') }}</label>
                            

                            <div class="col-md-6">
                            <input id="question7" type="text" class="form-control @error('question7') is-invalid @enderror" name="question7" value="{{ old('question7') }}" required autocomplete="question7" autofocus>
                              <small>If you have the name of the person referral you to Thailand Elite.</small>
                                @error('question7')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="question8" class="col-md-4 col-form-label text-md-right">{{ __('When do you plan to obtain your Elite Visa?') }}</label>
                            

                            <div class="col-md-6">
                            <input id="question8" type="text" class="form-control @error('question8') is-invalid @enderror" name="question8" value="{{ old('question8') }}" required autocomplete="question8" autofocus>
                              <small>If you have the name of the person referral you to Thailand Elite.</small>
                                @error('question8')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
               

                        <div class="form-group row">
                            <label for="upload_application" class="col-md-4 col-form-label text-md-right">{{ __('Upload Application Form (Available below)') }}</label>
                            

                            <div class="col-md-6">
                            <input id="upload_application" type="file" accept="image/*" multiple="multiple" class="form-control-file @error('upload_application') is-invalid @enderror" name="upload_application[]" required>
                              <small>Download the Application from below, print and fill up the required detail. Scan or take photo of all pages and upload here.</small>
                                @error('upload_application')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="upload_passport" class="col-md-4 col-form-label text-md-right">{{ __('Upload Passport') }}</label>
                            

                            <div class="col-md-6">
                            <input id="upload_passport" type="file" accept="image/*" multiple="multiple" class="form-control-file @error('upload_passport') is-invalid @enderror" name="upload_passport[]" required>
                              <small>Uploading a photo or scan copy of Passport (photo page). Full page with all detail visible. Please note: the file size limits for file upload fields are 100MB for a single file and 500MB in total for multiple files. In case the files are more than the limit, please use file sharing field below. </small>
                                @error('upload_passport')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="upload_share_link" class="col-md-4 col-form-label text-md-right">{{ __('File Sharing URL/Link') }}</label>
                            

                            <div class="col-md-6">
                            <input id="upload_share_link" type="text" class="form-control @error('upload_share_link') is-invalid @enderror" name="upload_share_link" required>
                              <small>Provide the Dropbox/Google Drive/WeTransfer or other File Sharing Link or URL inside the box if the upload file size are more than 500MB.</small>
                                @error('upload_share_link')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="upload_photo" class="col-md-4 col-form-label text-md-right">{{ __('Upload Photo (high resolution preferred)') }}</label>
                            

                            <div class="col-md-6">
                            <input id="upload_photo" type="file"accept="image/*" multiple="multiple"  class="form-control-file @error('upload_photo') is-invalid @enderror" name="upload_photo[]" required>
                              <small>High Resolution Photo (​Photo with ​blue & white background​ only​)</small>
                                @error('upload_share_link')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <hr>
                        <b class="col-md-8 col-form-label text-md-right mb-4">{{ __('Requirements to Enroll an Immediate Family Member') }}</b>
                        <div class="form-group row">
                       
                            <label for="upload_marriage_certificate" class="col-md-4 col-form-label text-md-right">{{ __('Upload Marriage Certificate (Family Applicant Only)') }}</label>
                            

                            <div class="col-md-6">
                            <input id="upload_marriage_certificate" type="file" accept="image/*" multiple="multiple" class="form-control-file @error('upload_marriage_certificate') is-invalid @enderror" name="upload_marriage_certificate[]" required>
                              <small>For uploading of Passport copy, Application Form, other relevant document only.</small>
                                @error('upload_marriage_certificate')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                    <div class="form-group row">
                       
                       <label for="upload_child_birth_certificate" class="col-md-4 col-form-label text-md-right">{{ __('Child Birth Certificate (Child Applicant Only)') }}</label>
                       

                       <div class="col-md-6">
                       <input id="upload_child_birth_certificate" type="file" accept="image/*" multiple="multiple" class="form-control-file @error('upload_child_birth_certificate') is-invalid @enderror" name="upload_child_birth_certificate[]" required>
                           @error('upload_child_birth_certificate')
                               <span class="invalid-feedback" role="alert">
                                   <strong>{{ $message }}</strong>
                               </span>
                           @enderror
                       </div>
                   </div>

                   <div class="form-group row">
                       
                       <label for="upload_others" class="col-md-4 col-form-label text-md-right">{{ __('Others') }}</label>
                       

                       <div class="col-md-6">
                       <input id="upload_others" type="file" accept="image/*" multiple="multiple" class="form-control-file @error('upload_others') is-invalid @enderror" name="upload_others[]" required>
                           @error('upload_others')
                               <span class="invalid-feedback" role="alert">
                                   <strong>{{ $message }}</strong>
                               </span>
                           @enderror
                       </div>
                   </div>

                   <div class="form-group row">
                       
                       <label for="comment" class="col-md-4 col-form-label text-md-right">{{ __('Comment') }}</label>
                       

                       <div class="col-md-6">
                       <textarea id="comment" type="text" class="form-control @error('comment') is-invalid @enderror" name="comment" required></textarea>
                       <small>Comment and additional matters of concern.</small>
                           @error('comment')
                               <span class="invalid-feedback" role="alert">
                                   <strong>{{ $message }}</strong>
                               </span>
                           @enderror
                       </div>
                   </div>

                   <b class="col-md-8 col-form-label text-md-right mb-4 text-justify">{{ __('I hereby giving consent for a clearance check should I have uploaded my copy of passport. You will expect to receive a letter of approval with payment detail of membership fee in due time. ') }}</b>


                   <div class="form-group">
                       <div class="col-md-12 text-center">
                            <label>
                                <input type="checkbox" name="agreement" value="yes"> I agree to receiving marketing and promotional materials.*</label>
                        </div>
                   </div>
                  
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4 mb-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
