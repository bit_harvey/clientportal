@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        <div class="row">
            <div class="col-sm-4 mb-4">
                <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Online Application</h5><hr>
                    <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    <a href="{{ route('application') }}" class="btn btn-primary">Online Registration</a>
                </div>
                </div>
            </div>
            <div class="col-sm-4 mb-4">
                <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Client Portal</h5><hr>
                    <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    <a href="{{ route('login') }}" class="btn btn-primary">Client Portal</a>
                </div>
                </div>
            </div>
            <div class="col-sm-4 mb-4">
                <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Sub-Agent Portal</h5><hr>
                    <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    <a href="{{ route('login') }}" class="btn btn-primary">Sub-Agent Portal</a>
                </div>
                </div>
            </div>
            
            </div>
        </div>
    </div>
</div>
@endsection
