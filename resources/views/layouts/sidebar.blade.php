
<ul class="list-group">
<div class="card-block">
    <li  onclick="location.href = '{{ route('home')}}';"  style="cursor:pointer" class="list-group-item"><span class="badge badge-dark mr-2"><i class="fa fa-home"></i></span> Dashboard</li>
    @if( auth::user()->role == 'Client')
    <li onclick="location.href = '{{ route('home')}}';"  style="cursor:pointer" class="list-group-item"><span class="badge badge-dark mr-2"><i class="fa fa-file"></i></span> My Documents</li>
    @else 
    <li onclick="location.href = '{{ route('applicant')}}';"  style="cursor:pointer" class="list-group-item"><span class="badge badge-dark mr-2"><i class="fa fa-file"></i></span> Application Registration</li>
    <li onclick="location.href = '{{ route('client-manager')}}';"  style="cursor:pointer"   class="list-group-item"><span class="badge badge-dark mr-2"><i class="fa fa-user"></i></span>  Client Manager</li>
    <li onclick="location.href = '';"  style="cursor:pointer" class="list-group-item"><span class="badge badge-dark mr-2"><i class="fa fa-list"></i></span> Application Listing</li>
    <li  onclick="location.href = '';"  style="cursor:pointer" class="list-group-item"><span class="badge badge-dark mr-2"><i class="fa fa-user"></i></span> User Manager</li>
    <li  onclick="location.href = '{{ route('admin')}}';"  style="cursor:pointer" class="list-group-item"><span class="badge badge-dark   mr-2"><i class="fa fa-cog"></i></span> Settings</li>
    <li  onclick="location.href = '';"  style="cursor:pointer" class="list-group-item"><span class="badge badge-dark mr-2"><i class="fa fa-book"></i></span> Announcement</li>
    <li  onclick="location.href = '';"  style="cursor:pointer" class="list-group-item"><span class="badge badge-dark mr-2"><i class="fa fa-list"></i></span> Audit Trail</li>
    @endif
    @if( auth::user()->role == 'Admin')
    @else 
    @endif
</ul>
  