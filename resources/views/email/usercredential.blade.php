<!DOCTYPE html>
<html>
<head>
    <title>Client Portal</title>
</head>
<body>
    <h1>Client Portal</h1>
    <p>This is your credential.</p>
   
    <p>Username: {{ $details['email'] }}</p>
    <p>Password: {{ $details['password'] }}</p>

    <p>Thank you</p>
</body>
</html>