
<!-- Modal -->
<div wire:ignore.self class="modal fade" id="adduser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
           <div class="modal-body">
            
                <form>
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" wire:model="name" value="" placeholder="name">
                @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label>Email Address</label>
                    <input type="text" class="form-control" wire:model="email" value="" placeholder="email">
                @error('email') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label>Role</label>
                    <select  wire:model="role" class="form-control">
                        <option value="">Select Role</option>
                        <option value="Agent">Agent</option>
                        <option value="Admin">Administrator</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Temporary Password</label>
                    <input type="text" wire:model="password" class="form-control" placeholder="password">
                    @error('password') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Close</button>
                <button type="button" wire:click.prevent="store()" class="btn btn-dark close-modal">Save changes</button>
            </div>
        </div>
    </div>
</div>