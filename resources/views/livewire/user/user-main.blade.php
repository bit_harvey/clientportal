@extends('layouts.app')

@section('content')
<div class="container">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="main-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('admin')}}">Administrator</a></li>
              <li class="breadcrumb-item active">User</li>
            </ol>
          </nav>
          <!-- /Breadcrumb -->
    <div class="row">
        
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('User') }} 
                
                <button type="button" class="btn btn-dark float-right" data-toggle="modal" data-target="#adduser">
                    + Add User
                </button>

                </div>
                            @if (session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session('message') }}
                                </div>
                            @endif
                            <livewire:user.user-table />
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
