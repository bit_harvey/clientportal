<!-- Modal -->
<div wire:ignore.self class="modal fade" id="approvedmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
       <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Approve</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
             <h3 class="mb-3">Are you sure you want to approve this application?</h3>
                <form class="form-group">
                <input type="hidden" class="form-control" wire:model="client_id" placeholder="client_id">
                @error('client_id') <span class="text-danger error" >{{ $message }}</span>@enderror
                <input type="hidden" class="form-control" wire:model="name" placeholder="name">
                @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                <label for="">Email:</label>
                <input type="text" class="form-control" wire:model="email" placeholder="email" readonly>
                @error('email') <span class="text-danger error">{{ $message }}</span>@enderror
                <input type="hidden" wire:model="role" class="form-control">
                @error('role') <span class="text-danger error">{{ $message }}</span>@enderror
                <input type="hidden" class="form-control" wire:model="active" placeholder="active">
                @error('active') <span class="text-danger error">{{ $message }}</span>@enderror
                <br>
                <label for="">Password:</label>
                <input type="text" wire:model="password" class="form-control" placeholder="password" required>
               @error('password') <span class="text-danger error">{{ $message }}</span>@enderror
                </form>
            </div>
           
            <div class="modal-footer">
                <button type="button" wire:click.prevent="cancel()" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" wire:click.prevent="approved_store()" class="btn btn-success" data-dismiss="modal"><i class="fa fa-check"> </i>Approve</button>
            </div>
       </div>
    </div>
</div>