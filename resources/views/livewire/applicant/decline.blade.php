<!-- Decline -->
<div wire:ignore.self class="modal fade" id="declinemodal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Attention!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <input type="hidden" class="form-control @error('client_id') is-invalid @enderror" wire:model="client_id">
        Are you sure you want to decline this application? 
        <br><br>
        This can't be undone.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" wire:click.prevent="decline_store()" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-ban"></i> Decline</button>

      </div>
    </div>
  </div>
</div>