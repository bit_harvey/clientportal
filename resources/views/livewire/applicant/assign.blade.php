<!-- Modal -->
<div wire:ignore.self class="modal fade" id="assignmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
       <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Assign</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                <input type="hidden" class="form-control @error('client_id') is-invalid @enderror" wire:model="client_id">
                <select class="form-control @error('agent_id') is-invalid @enderror" wire:model="agent_id"  required>
                    <option value="None">Select Agent</option>
                        @foreach($agents as $agent)
                            <option value="{{ $agent->id }}">{{ $agent->name }}</option>
                        @endforeach
                </select>
                </form>
            </div>
           
            <div class="modal-footer">
                <button type="button" wire:click.prevent="cancel()" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" wire:click.prevent="assign_store()" class="btn btn-info" data-dismiss="modal"><i class="fa fa-check"> </i>Assign</button>
            </div>
       </div>
    </div>
</div>