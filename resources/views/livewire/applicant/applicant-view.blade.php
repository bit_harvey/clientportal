@extends('layouts.app')

@section('content')
<div class="container">
    <div class="main-body">
    
          <!-- Breadcrumb -->
          <nav aria-label="breadcrumb" class="main-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('applicant')}}">Application</a></li>
              <li class="breadcrumb-item active" aria-current="page">Manage</li>
            </ol>
          </nav>
          <!-- /Breadcrumb -->
    
          <div class="row">
          @if (session()->has('message'))
              <div class="alert alert-success" style="margin-top:30px;">x
                {{ session('message') }}
              </div>
          @endif
            <div class="col-md-12">
           
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Personal Information
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        <div class="row">
                        
                            <div class="col-sm-3">
                                <h6 class="mb-0">Full Name</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                            @foreach($applicants as $applicant)
                                {{ $applicant->fname }} {{ $applicant->lname }} 
                                @if ($applicant->status  == 0)
                                    <span class="badge badge-warning">Pending</span>
                                @elseif ($applicant->status  == 1)
                                     <span class="badge badge-success">Approved</span>
                                @else ($applicant->status  == 2)
                                    <span class="badge badge-danger">Declined</span>
                                @endif
                                
                            </div>
                        </div>
                    <hr>
                    <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Email</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    {{ $applicant->email }} 
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Nationality</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    {{ $applicant->nationality }} 
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Phone Number</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                     ({{ $applicant->country_number }})  {{ $applicant->phone_number }} 
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Address</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    {{ $applicant->resedential_address }} {{ $applicant->location }}  {{ $applicant->province }} 
                    </div>
                  </div>

                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Program</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    {{ $applicant->program }} 
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Agent</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    {{ $applicant->umail }} 
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Assignee</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    {{ $applicant->assignee }} 
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Current Visa</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    {{ $applicant->current_visa }} 
                    </div>
                  </div>
                  @endforeach

                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Questions
                        </button>
                    </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card-body">
                    <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Question 1</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    @foreach($applicants as $applicant)
                    {{ $applicant->question1 }} 
                    </div>
                  </div>

                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Question 2</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    {{ $applicant->question2 }} 
                    </div>
                  </div>

                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Question 3</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    {{ $applicant->question3 }} 
                    </div>
                  </div>
                  
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Question 4</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    {{ $applicant->question4 }} 
                    </div>
                  </div>

                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Question 5</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    {{ $applicant->question5 }} 
                    </div>
                  </div>

                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Question 6</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    {{ $applicant->question6 }} 
                    </div>
                  </div>

                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Question 6</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    {{ $applicant->question6 }} 
                    </div>
                  </div>

                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Question 7</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    {{ $applicant->question7 }} 
                    </div>
                  </div>

                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Question 8</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    {{ $applicant->question8 }} 
                    </div>
                  </div>
                  @endforeach
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Uploaded Files
                        </button>
                    </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                    <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Application</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    @foreach($applicants as $applicant)
                    @foreach (json_decode($applicant->upload_application, true) as $upload_application)
                      <img src="{{ url('files/' . $upload_application) }}" heigh="50" width="50">
                    @endforeach
                    </div>
                  </div>

                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Passport</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    @foreach (json_decode($applicant->upload_passport, true) as $upload_passport)
                      <img src="{{ url('files/' . $upload_passport) }}" heigh="50" width="50">
                    @endforeach
                    </div>
                  </div>

                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Link</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    {{ $applicant->upload_share_link }} 
                    </div>
                  </div>

                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Photo</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    @foreach (json_decode($applicant->upload_photo, true) as $upload_photo)
                      <img src="{{ url('files/' . $upload_photo) }}" heigh="50" width="50">
                    @endforeach
                    </div>
                  </div>

                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Marriage Certificate</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    @foreach (json_decode($applicant->upload_marriage_certificate, true) as $upload_marriage_certificate)
                      <img src="{{ url('files/' . $upload_marriage_certificate) }}" heigh="50" width="50">
                    @endforeach
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Child Birth Certificate</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    @foreach (json_decode($applicant->upload_child_birth_certificate, true) as $upload_child_birth_certificate)
                      <img src="{{ url('files/' . $upload_child_birth_certificate) }}" heigh="50" width="50">
                    @endforeach
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Others</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    @foreach (json_decode($applicant->upload_others, true) as $upload_others)
                      <img src="{{ url('files/' . $upload_others) }}" heigh="50" width="50">
                    @endforeach
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Comment</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    {{ $applicant->comment }} 
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Agreement</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                    {{ $applicant->agreement }} 
                    </div>
                  </div>
                  
                  @endforeach
                    </div>
                    </div>
                </div>
                </div>
     
             
                </div>
              </div>
            </div>
          </div>
        </div>

        
    </div>



@endsection