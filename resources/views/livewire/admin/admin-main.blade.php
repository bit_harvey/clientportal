@extends('layouts.app')

@section('content')
<div class="container">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="main-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
              <li class="breadcrumb-item active">Administrator</li>
            </ol>
          </nav>
          <!-- /Breadcrumb -->
    <div class="row">
        
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Administrator') }} </div>
                     <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item"> <a href="{{ route('user') }}"><i class="fa fa-user"></i> User</a> </li>
                            <li class="list-group-item"> <a href="{{ route('email') }}"><i class="fa fa-envelope"></i> Email</a> </li>
                        </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
