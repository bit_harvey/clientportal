@extends('layouts.app')

@section('content')
<div class="container">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="main-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
              <li class="breadcrumb-item active">Client</li>
            </ol>
          </nav>
          <!-- /Breadcrumb -->
    <div class="row">
        
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Client') }} </div>
                <table class="card-table table">
                        <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($members as $member)
                        <tr>
                            <td>{{ $member->id }}</td>
                            <td>{{ $member->fname }} {{ $member->lname }}</td>
                            <td>{{ $member->email }}</td>
                            <td>
                                @if ($member->status  == 0)
                                    <span class="badge badge-warning">Pending</span>
                                @elseif ($member->status  == 1) 
                                        <span class="badge badge-success">Approved</span>
                                @else ($member->status  == 2)
                                    <span class="badge badge-danger">Declined</span>
                                @endif
                            </td>
                            <td><a href="" class="btn btn-primary">View</a></td>
                        </tr>
                       @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
