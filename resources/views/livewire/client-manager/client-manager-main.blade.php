@extends('layouts.app')

@section('content')
<div class="container">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="main-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ route('home')}}">Dashboard</a></li>
              <li class="breadcrumb-item active">Client Manager
              </li>
            </ol>
          </nav>
          <!-- /Breadcrumb -->
    <div class="row">
        
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h5 class="mb-0">{{ __('Client Manager') }}<a href="{{ route('add-application') }}" type="button" class="btn btn-dark float-right">+ Add Application</a></h5></h5>
                    <small class="text-muted">About of Client Manager</small>
                 </div>
                    <livewire:client-manager.client-manager-table/>
                </div>
                <div class="card-footer text-muted">
                    Thailand Elite: Authorized General Sales and Services Agent
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
