@extends('layouts.app')

@section('content')
<div class="container">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="main-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ route('home')}}">Dashboard</a></li>
              <li class="breadcrumb-item active"><a href="{{ route('client-manager')}}">Client Manager</a></li>
              <li class="breadcrumb-item active">Update Client
              </li>
            </ol>
          </nav>
          <!-- /Breadcrumb -->
    <div class="row">
        
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h5 class="mb-0">{{ __('Update Client') }}</h5>
                    <small class="text-muted">About of Client Manager</small>
                 </div>
                    <div class="card-body">
                        <livewire:client-manager.client-manager-edit :edit="$client_id"/>
                    </div>
                </div>
                <div class="card-footer text-muted">
                    Thailand Elite: Authorized General Sales and Services Agent
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
