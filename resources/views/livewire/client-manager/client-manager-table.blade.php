
<div>
@if (session()->has('message'))
        <div class="alert alert-success" style="margin-top:30px;">x
        {{ session('message') }}
        </div>
@endif
@if ($errors->any())
    @foreach ($errors->all() as $error)
    <div class="alert alert-danger">
      {{ $error }}
    </div>
    @endforeach
@endif
<div class="table-responsive">
    <table class="card-table table">
        <thead>
        <tr>
            <th scope="col"></th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Agent</th>
            <th scope="col">Assignee</th>
            <th scope="col">Status</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
            @foreach($members as $member)
            <tr>
                <td>{{ $member->id }}</td>
                <td>{{ $member->fname }} {{ $member->lname }}</td>
                <td>{{ $member->email }}</td>
                @if ($member->umail == '')
                        <td>None</td>
                    @else 
                        <td>{{ $member->umail }}</td>  
                @endif
                <td>{{ $member->assignee }}</td>
                <td>
                @if ($member->status  == 0)
                    <span class="badge badge-warning">Pending</span>
                @elseif ($member->status  == 1)
                        <span class="badge badge-success">Approved</span>
                @else ($member->status  == 2)
                    <span class="badge badge-danger">Declined</span>
                @endif
                </td>
                <td>
                        <a href="{{route('applicant.view', [$member->id])}}" class="btn btn-dark"><i class="fa fa-eye"></i> </a>
                        <a href="{{route('client-manager.view', [$member->id])}}" class="btn btn-dark"><i class="fa fa-edit"></i> </a>
                        <a href="" class="btn btn-dark"><i class="fa fa-file"></i> </a>
                            
                </td>
                
            </tr>
            @endforeach
        </tbody>
    </table>
</div>